package controllers;

import dao.InfoDAO;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.ListView;
import javafx.scene.control.MultipleSelectionModel;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

import java.io.IOException;
import java.sql.SQLException;


public class MenuController {
    InfoDAO infoDao = new InfoDAO();

    @FXML
    private ListView<String> genresView;
    @FXML
    private ListView<String> bookView;

    @FXML
    public void initialize() {
        try {
            ObservableList<String> genres = FXCollections.observableArrayList(infoDao.getAllGenres());
            genresView.setItems(genres);
            MultipleSelectionModel<String> genresSelection = genresView.getSelectionModel();
            //слушатель для отслеживания выбора жанра
            genresSelection.selectedItemProperty().addListener(new ChangeListener<String>() {
                @Override
                public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                    try {
                        ObservableList<String> selectionGenres = FXCollections.observableArrayList(infoDao.getSearchBy(newValue, "Жанр"));
                        bookView.setItems(selectionGenres);
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                }
            });

            allGenres();

            MultipleSelectionModel<String> bookSelection = bookView.getSelectionModel();
            //слушатель для отслеживания выбора книги
            bookSelection.selectedItemProperty().addListener(new ChangeListener<String>() {
                @Override
                public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                    try {
                        PageBookController pageBookController = new PageBookController();
                        Stage primaryStage = new Stage();
                        String[] arrData = parseData(newValue);

                        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/pagebook.fxml"));

                        pageBookController.setNameBookSt(arrData[0]);
                        pageBookController.setAuthorSt(arrData[1]);
                        pageBookController.setGenreSt(arrData[2]);
                        pageBookController.setYearSt(arrData[3]);


                        fxmlLoader.setController(pageBookController);

                        Parent root = fxmlLoader.load();
                        primaryStage.setTitle("Информация");
                        primaryStage.setScene(new Scene(root, 389, 400));
                        primaryStage.show();

                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                }
            });

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private String[] parseData(String value) {
        return value.split(" {2}");
    }

    /**
     * Выводит книги всех жанров(сброс фильтров)
     *
     * @throws SQLException - ошибка с базой данных или sql запросами
     */
    public void allGenres() throws SQLException {

        ObservableList<String> book = FXCollections.observableArrayList(infoDao.getInfoBook());
        bookView.setItems(book);

    }

    @FXML
    private TextField nameBook;
    @FXML
    private TextField author;

    /**
     * Поикс книг по названию или по автору
     */
    public void searchBy() {
        try {
            if (!author.getText().equals("")) {
                ObservableList<String> selectionAuthor = FXCollections.observableArrayList(infoDao.getSearchBy(author.getText(), "Автор"));
                bookView.setItems(selectionAuthor);
            }

            if (!nameBook.getText().equals("")) {
                ObservableList<String> selectionNameBook = FXCollections.observableArrayList(infoDao.getSearchBy(nameBook.getText(), "Название книги"));
                bookView.setItems(selectionNameBook);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

}
