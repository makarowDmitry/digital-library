package controllers;

import dao.ContentDAO;
import javafx.fxml.FXML;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.sql.SQLException;


public class TextController {
    @FXML
    private Button next;
    @FXML
    private Button back;
    @FXML
    private Label page;
    @FXML
    private Label text;

    private String nameBook;
    ContentDAO contentDAO = new ContentDAO();


    @FXML
    public void initialize() {

        text.setPadding(new Insets(10, 0, 0, 30));

        try {
            String urlPage = contentDAO.getData(nameBook, 3);
            String textBook = parseText(readTextFromSite(urlPage, contentDAO.getData(nameBook,4)));
            text.setText(textBook);
            displayLabelPage();
            next.setOnAction(nextPageEvent -> {
                try {
                    String pageNext = parseText(readTextFromSite(nextP(), contentDAO.getData(nameBook,4)));
                    text.setText(pageNext);
                    displayLabelPage();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            });

            back.setOnAction(backPageEvent -> {
                try {
                    String pageBack = parseText(readTextFromSite(backP(), contentDAO.getData(nameBook,4)));
                    text.setText("");
                    text.setText(pageBack);
                    displayLabelPage();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            });
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Задает значение для label предназначенный для отображения количества страниц в книге
     *
     * @throws SQLException - ошибка с базой данных или sql запросами
     */
    private void displayLabelPage() throws SQLException {
        String textLabelPage =i + " из " + contentDAO.getData(nameBook,5);
        page.setText(textLabelPage);
    }

    private int i = 1;

    /**
     * Меняет url ссылку, для смены текста на следуюшую страницу
     *
     * @return - url ссылка
     * @throws SQLException - ошибка с базой данных или sql запросами
     */
    private String nextP() throws SQLException {
        i++;
        String urlPage = null;
        if(i < Integer.parseInt(contentDAO.getData(nameBook,5))) {
            urlPage = contentDAO.getData(nameBook,3) + i + "/";
        }else {
            i--;
            urlPage = contentDAO.getData(nameBook,3) + i +"/";
        }
        return urlPage;
    }

    /**
     * Меняет url ссылку, для смены текста на предыдущую страницу
     *
     * @return - url ссылка
     * @throws SQLException - ошибка с базой данных или sql запросами
     */
    private String backP() throws SQLException {
        String urlPage = null;
        i--;
        if (i >= 1) {
            urlPage = contentDAO.getData(nameBook,3) + i + "/";
        } else {
            i++;
            urlPage = contentDAO.getData(nameBook,3) + i + "/";
        }
        return urlPage;
    }

    /**
     * Обработка html кода с сайта
     *
     * @param url - ссылка на сайт
     * @param tag - в каких тегах брать данные
     * @return - текст книги
     */
    private String readTextFromSite(String url, String tag) {
        StringBuilder text = new StringBuilder();
        try {
            Document document = Jsoup.connect(url).get();
            Elements elementsBook = document.select(tag);
            for (Element element : elementsBook) {
                text.append(element.select("p"));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return text.toString();
    }

    private String parseText(String text) {
        text = text.replace("&nbsp;", "");
        text = text.replace("<p>", "");
        text = text.replace("</p>", "");
        text = text.replace("<e>", "");
        text = text.replace("</e>", "");

        return text;
    }

    public void setNameBook(String nameBook) {
        this.nameBook = nameBook;
    }


}
