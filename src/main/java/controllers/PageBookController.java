package controllers;

import dao.ContentDAO;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import downloadfile.DownloadFile;
import javafx.stage.Stage;

import java.io.IOException;
import java.sql.SQLException;

public class PageBookController {

    @FXML
    private Label nameBook;
    @FXML
    private Label author;
    @FXML
    private Label genre;
    @FXML
    private Label year;

    private String nameBookSt;
    private String authorSt;
    private String genreSt;
    private String yearSt;

    @FXML
    private Button download;
    @FXML
    private Button read;

    public void setNameBookSt(String nameBookSt) {
        this.nameBookSt = nameBookSt;
    }

    public void setAuthorSt(String authorSt) {
        this.authorSt = authorSt;
    }

    public void setGenreSt(String genreSt) {
        this.genreSt = genreSt;
    }

    public void setYearSt(String yearSt) {
        this.yearSt = yearSt;
    }

    @FXML
    public void initialize() {
        nameBook.setText(nameBookSt);
        author.setText(authorSt);
        genre.setText(genreSt);
        year.setText(yearSt);
        //setImage();
        download.setOnAction(downloadEvent -> {
            downloadText();
        });
        read.setOnAction(readEvent -> {
            readText();
        });
    }

    /**
     * Слушатель на кнопку "скачать книгу", вызывает метыь
     */
    private void downloadText() {
        DownloadFile downloadFile = new DownloadFile();
        ContentDAO contentDAO = new ContentDAO();
        try {
            downloadFile.downloadFile(contentDAO.getUrlDownload(nameBookSt));
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Запускает окно для чтения текста книги
     */
    private void readText() {
        TextController textController = new TextController();
        Stage primaryStage = new Stage();

        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/text.fxml"));

        textController.setNameBook(nameBookSt);

        fxmlLoader.setController(textController);
        try {
            Parent root = fxmlLoader.load();
            primaryStage.setTitle("Текст книги");
            primaryStage.setScene(new Scene(root, 1200, 720));
            primaryStage.show();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

}
