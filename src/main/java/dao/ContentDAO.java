package dao;

import database.DB;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class ContentDAO {
     DB db = new DB();

    /**
     * SQL запросом возвращает url ссылку для скачивания книги
     *
     * @param nameBook - название книги
     * @return - url ссылку
     * @throws SQLException - ошибка с базой данных или sql запросами
     */
    public ArrayList<String> getUrlDownload(String nameBook) throws SQLException {
        db.connect();
        ResultSet resultSet = db.stat.executeQuery("SELECT * FROM `content` WHERE `Название книги` = '" + nameBook + "'");
        ArrayList<String> dataBook = new ArrayList<>();
        while (resultSet.next()) {
            dataBook.add(resultSet.getString(2));
        }
        dataBook.add(nameBook);
        db.close();
        return dataBook;
    }

    /**
     * Возвращает данные по SQL запросу из таблицы "content"
     *
     * @param nameBook - название книги
     * @param numColumn - номер столбца
     * @return - данные из ячейки
     * @throws SQLException - ошибка с базой данных или sql запросами
     */
    public String getData(String nameBook, int numColumn) throws SQLException {
        db.connect();
        ResultSet resultSet = db.stat.executeQuery("SELECT * FROM `content` WHERE `Название книги` = '" + nameBook + "'");
        String url = "";
        while (resultSet.next()) {
            url = resultSet.getString(numColumn);
        }
        db.close();
        return url;
    }


}
