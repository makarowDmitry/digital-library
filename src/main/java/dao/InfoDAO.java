package dao;

import database.DB;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class InfoDAO {

    DB db = new DB();

    /**
     * Возвращает список всех жанров с помощью SQL запроса
     *
     * @return - список жанров
     * @throws SQLException - ошибка с базой данных или sql запросами
     */
    public ArrayList<String> getAllGenres() throws SQLException {
        db.connect();
        ResultSet resultSet = db.stat.executeQuery("SELECT `Жанр` FROM `info`");
        ArrayList<String> arrayList = new ArrayList<>();
        while (resultSet.next()) {
            arrayList.add(resultSet.getString(1));
        }
        db.close();
        return arrayList;
    }

    /**
     * Возваращает книгу из базы данных
     *
     * @param newValue - значение ячейки
     * @param column - название столбца
     * @return - данные всей найденной строки
     * @throws SQLException - ошибка с базой данных или sql запросами
     */
    public ArrayList<String> getSearchBy(String newValue, String column) throws SQLException {
        db.connect();
        ResultSet resultSet = db.stat.executeQuery("SELECT * FROM `info` WHERE `" + column + "` = '" + newValue + "'");
        ArrayList<String> nameBook = new ArrayList<>();
        ArrayList<String> nameAuthor = new ArrayList<>();
        ArrayList<String> genre = new ArrayList<>();
        ArrayList<String> year = new ArrayList<>();
        while (resultSet.next()) {
            nameBook.add(resultSet.getString(1) + "  ");
            nameAuthor.add(resultSet.getString(2) + "  ");
            genre.add(resultSet.getString(3) + "  ");
            year.add(resultSet.getString(4) + "  ");
        }
        db.close();
        ArrayList<String> arrayList = new ArrayList<>();
        for (int i = 0; i < nameBook.size(); i++) {
            arrayList.add(nameBook.get(i) + nameAuthor.get(i) + genre.get(i) + year.get(i));
        }
        return arrayList;
    }

    /**
     * Возвращает все данные из таблицы "info"
     *
     * @return - список всех данных
     * @throws SQLException - ошибка с базой данных или sql запросами
     */
    public ArrayList<String> getInfoBook() throws SQLException {
        db.connect();
        ResultSet resultSet = db.stat.executeQuery("SELECT * FROM `info`");
        ArrayList<String> nameBook = new ArrayList<>();
        ArrayList<String> nameAuthor = new ArrayList<>();
        ArrayList<String> genre = new ArrayList<>();
        ArrayList<String> year = new ArrayList<>();
        while (resultSet.next()) {
            nameBook.add(resultSet.getString(1) + "  ");
            nameAuthor.add(resultSet.getString(2) + "  ");
            genre.add(resultSet.getString(3) + "  ");
            year.add(resultSet.getString(4));
        }
        db.close();
        ArrayList<String> arrayList = new ArrayList<>();
        for (int i = 0; i < nameBook.size(); i++) {
            arrayList.add(nameBook.get(i) + nameAuthor.get(i) + genre.get(i) + year.get(i));
        }

        return arrayList;
    }

}
