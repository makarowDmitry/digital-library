package database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class DB {
    protected Connection conn;
    public Statement stat;

    public void connect(){
        try {
            conn = DriverManager.getConnection("jdbc:mysql://localhost/book?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC", "mysql", "mysql");
            stat = conn.createStatement();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void close() throws SQLException {
        stat.close();
        conn.close();
    }

}
