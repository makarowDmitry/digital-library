package downloadfile;

import java.io.*;
import java.net.URL;
import java.util.ArrayList;

public class DownloadFile {
    /**
     * Скачивает zip архив по ссылке
     *
     * @param data - список состоящий из ссылки на фаил и будущего названия файла
     */
    public void downloadFile(ArrayList<String> data) {
        try {
            URL url = new URL(data.get(0));
            BufferedInputStream bis = new BufferedInputStream(url.openStream());
            FileOutputStream fos = new FileOutputStream("C:\\Users\\10\\IdeaProjects\\Digital library\\src\\main\\resources\\receivedfile\\" + data.get(1) + ".zip");
            byte[] buffer = new byte[1024];
            int count = 0;
            while ((count = bis.read(buffer, 0, 1024)) != -1) {
                fos.write(buffer, 0, count);
            }
            fos.close();
            bis.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
