package dao;

import org.junit.Test;

import java.sql.SQLException;
import java.util.ArrayList;

import static org.junit.Assert.*;

public class ContentDAOTest {

    ContentDAO contentDAO = new ContentDAO();

    @Test
    public void getUrlDownloadTest(){
        try {
           ArrayList<String> arrayList = contentDAO.getUrlDownload("Крысы в стенах");
           ArrayList<String> arrayList1 = new ArrayList<>();
           arrayList1.add("https://litportal.ru/trial/txt/644595.txt.zip");
           arrayList1.add("Крысы в стенах");
           assertEquals(arrayList1.get(0),arrayList.get(0));
           assertEquals(arrayList1.get(1),arrayList.get(1));
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void getDataTagTest(){
        try {
            String tag = contentDAO.getData("Крысы в стенах",4);
            assertEquals("div.book-reader",tag);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void getDataPageTest(){
        try {
            String page = contentDAO.getData("Крысы в стенах",5);
            assertEquals("8",page);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void getDataUrlBookReadTest(){
        try {
            String page = contentDAO.getData("Утраченный символ",3);
            assertEquals("https://bookzip.ru/reader/7/",page);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

}