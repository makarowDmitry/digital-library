package dao;

import org.junit.Test;

import java.sql.SQLException;
import java.util.ArrayList;

import static org.junit.Assert.*;

public class InfoDAOTest {
    InfoDAO infoDAO = new InfoDAO();

    @Test
    public void getAllGenresTest() {
        try {
            ArrayList<String> arrayList = infoDAO.getAllGenres();
            ArrayList<String> arrayList1 = new ArrayList<>();
            arrayList1.add("Историческая драма");
            arrayList1.add("Детектив");
            arrayList1.add("Религия");
            assertEquals(arrayList1.get(0), arrayList.get(0));
            assertEquals(arrayList1.get(1), arrayList.get(4));
            assertEquals(arrayList1.get(2), arrayList.get(7));
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void getSearchByNameBookTest() {
        try {
            ArrayList<String> arrayList = infoDAO.getSearchBy("Капитанская дочка", "Название книги");
            ArrayList<String> arrayList1 = new ArrayList<>();
            arrayList1.add("Капитанская дочка  А. С. Пушкин  Историческая драма  1836  ");
            assertEquals(arrayList1.get(0), arrayList.get(0));
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void getSearchByAuthorTest() {
        try {
            ArrayList<String> arrayList = infoDAO.getSearchBy("Р. Пишель", "Автор");
            ArrayList<String> arrayList1 = new ArrayList<>();
            arrayList1.add("Будда, его жизнь и учение  Р. Пишель  Религия  2009  ");
            assertEquals(arrayList1.get(0), arrayList.get(0));
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}